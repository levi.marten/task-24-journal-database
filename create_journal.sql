USE journals_db;

CREATE TABLE `journal` (
	id INT PRIMARY KEY AUTO_INCREMENT NOT NULL, 
    name VARCHAR(128) NOT NULL,
    created_at DATETIME NOT NULL DEFAULT NOW(),
    updated_at DATETIME DEFAULT NULL,
    Active BOOLEAN DEFAULT true
);
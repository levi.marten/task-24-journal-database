UPDATE journal_entry SET title = "Why CSS matters", updated_at = now() WHERE id = 1;

UPDATE journal_entry  SET text = "This text is about how to use javascript with databases. JSON, MongoDB, NoSQL and Sequelize for the win. Also, love.", author_id = 2, updated_at = now() WHERE id = 2;

UPDATE journal_entry  SET author_id = 2, updated_at = now() WHERE id = 3;
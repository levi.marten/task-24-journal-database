INSERT INTO `journal_entry` (title, text, author_id, journal_id)
VALUES ("My first blog post about CSS", "My first written text with a bunch of awesome information about CSS.", 1, 3);

INSERT INTO `journal_entry` (title, text, author_id, journal_id)
VALUES ("Inserting data in a database", "This text is about how to use javascript with databases. JSON, MongoDB, NoSQL and Sequelize for the win.", 2, 2);

INSERT INTO `journal_entry` (title, text, author_id, journal_id)
VALUES ("We are all going to die", "In this post I am going to talk about the Corona virus and how we will soon see it fade out of memory, but how we will all anyway die someday. But that is just one day. Every other day we are not going to die.",(select id from author where id=3), (select id from journal where id=3));

INSERT INTO `journal_entry` (title, text, author_id, journal_id)
VALUES ("The Gods Must Be Crazy", "Have you ever seen this movie about a Coca-Cola bottle falling from the sky, leading to all kidns of crazy things happening?", 2, 2);
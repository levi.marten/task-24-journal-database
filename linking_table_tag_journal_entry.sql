/* Linking table for tag and journal_entry */

CREATE TABLE `tag_journal_entry` (
    journal_entry_id INT NOT NULL,
    tag_id INT NOT NULL,
    FOREIGN KEY (journal_entry_id)
        REFERENCES journal_entry(id),
    FOREIGN KEY (tag_id)
        REFERENCES tag(id)
);
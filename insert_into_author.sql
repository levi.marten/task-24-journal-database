INSERT INTO `author` (name, surname, email, profile_description)
VALUES ("Levi", "Mårten", "levi.marten@scania.com", "This guy knows what he is writing about. Definitely an awesome dude.");

INSERT INTO `author` (name, surname, email, profile_description)
VALUES ("Charles-Ingvar", "Jönsson", "sickan@jonssonligan.se", "This is a criminal mastermind. No one like this guy to tell you about hacking things.");

INSERT INTO `author` (name, surname, email, profile_description)
VALUES ("Donald", "Duck", "donald@duckburg.com", "What's the matter with you?");
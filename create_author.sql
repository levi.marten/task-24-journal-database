USE journals_db;

CREATE TABLE `author` (
	id INT PRIMARY KEY AUTO_INCREMENT NOT NULL, 
    name VARCHAR(128) NOT NULL,
    surname VARCHAR(128) NOT NULL,
	email VARCHAR(128) NOT NULL,
	profile_description LONGTEXT,
    created_at DATETIME NOT NULL DEFAULT NOW(),
    updated_at DATETIME DEFAULT NULL,
    Active BOOLEAN DEFAULT true
);
USE journals_db;

CREATE TABLE `journal_entry` (
	id INT PRIMARY KEY AUTO_INCREMENT NOT NULL, 
    title VARCHAR(128) NOT NULL,
    text LONGTEXT NOT NULL,
    created_at DATETIME NOT NULL DEFAULT NOW(),
    updated_at DATETIME DEFAULT NULL,
    Active BOOLEAN DEFAULT true,
    
	author_id INT NOT NULL,
    journal_id INT NOT NULL,
    FOREIGN KEY (author_id) REFERENCES author(id),
	FOREIGN KEY (journal_id) REFERENCES journal(id)
);
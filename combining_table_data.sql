SELECT author.name, journal_entry.title FROM author JOIN journal_entry ON author.id = journal_entry.author_id;
SELECT COUNT(journal_entry.title), author.name, author.surname FROM journal_entry JOIN author ON author.id = journal_entry.author_id WHERE author.id = 3;

SELECT tag.name, journal_entry_id FROM tag JOIN tag_journal_entry ON tag.id = tag_journal_entry.tag_id;
SELECT journal_entry.title, journal_entry_id FROM journal_entry JOIN tag_journal_entry ON journal_entry.id = tag_journal_entry.journal_entry_id;

SELECT journal_entry.title, GROUP_CONCAT(tag.name) AS tag FROM journal_entry LEFT JOIN tag_journal_entry ON journal_entry.id = tag_journal_entry.journal_entry_id LEFT JOIN tag ON tag.id = tag_journal_entry.tag_id  GROUP BY journal_entry.title;

SELECT journal_entry.title as BlogTitle, author.name AS WriterName, author.surname AS WriterSurname, GROUP_CONCAT(tag.name) AS tag 
FROM journal_entry 
LEFT JOIN tag_journal_entry ON journal_entry.id = tag_journal_entry.journal_entry_id 
LEFT JOIN tag ON tag.id = tag_journal_entry.tag_id 
LEFT JOIN author ON author.id = journal_entry.author_id
GROUP BY journal_entry.title;

SELECT COUNT(tag.id) AS TagCount, author.name AS AuthorName, author.surname AS AuthorLastname
FROM journal_entry
LEFT JOIN tag_journal_entry ON tag_journal_entry.journal_entry_id = journal_entry.id 
LEFT JOIN tag ON tag_journal_entry.tag_id = tag.id
LEFT JOIN author ON author.id = journal_entry.author_id;

SELECT journal_entry.title as BlogTitle, journal.name AS BlogCategory, author.name AS WriterName, author.surname AS WriterSurname, GROUP_CONCAT(tag.name) AS tag 
FROM journal_entry 
LEFT JOIN tag_journal_entry ON journal_entry.id = tag_journal_entry.journal_entry_id 
LEFT JOIN journal ON journal.id = journal_entry.journal_id 
LEFT JOIN tag ON tag.id = tag_journal_entry.tag_id 
LEFT JOIN author ON author.id = journal_entry.author_id
GROUP BY journal_entry.title;

SELECT COUNT(journal_entry.id) AS Entries, journal.name AS Category
FROM journal_entry
LEFT JOIN journal ON journal_entry.journal_id = journal.id
WHERE journal.id = 3;

SELECT COUNT(journal_entry.id) AS Entries, author.name AS Writer
FROM journal_entry
LEFT JOIN author ON journal_entry.author_id = author.id
WHERE author.id = 1;





